//
//  Usuario.swift
//  CoreDataExample
//
//  Created by iOS 20 on 19/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import Foundation

class Usuario: NSObject {
    
    var name: String?
    var city: String?
    
    init (name:String, city:String) {
        super.init()
        self.name = name
        self.city = city
    }
    
    
}